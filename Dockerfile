FROM openjdk:8-alpine
ADD /build/libs/onlinebookstore-0.0.1-SNAPSHOT.jar onlinebookstore.jar
ENTRYPOINT ["java","-jar","/onlinebookstore.jar"]